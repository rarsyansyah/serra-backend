import EntityBaseModel from '../config/entity_base_model';
import { Entity, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Event extends EntityBaseModel {
  @Column({ length: 100 })
  name: string;

  @Column({ length: 125 })
  slug: string;

  @Column()
  description: string;

  @Column({ type: 'time with time zone' })
  startedAt: Date;

  @Column({ type: 'time with time zone' })
  endedAt: Date;

  @ManyToOne(() => User, (user) => user.id)
  user: User;
}
