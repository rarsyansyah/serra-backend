import EntityBaseModel from '../config/entity_base_model';
import { Entity, Column } from 'typeorm';

@Entity()
export class Division extends EntityBaseModel {
  @Column({ length: 50 })
  name: string;

  @Column({ length: 75 })
  slug: string;
}
