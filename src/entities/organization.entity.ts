import { Village } from './villages.entity';
import { Gender } from '../helper/enum_list';
import EntityBaseModel from '../config/entity_base_model';
import { Entity, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Organization extends EntityBaseModel {
  @Column({ length: 100 })
  address: string;

  @Column({ length: 50, unique: true })
  secretariatEmail: string;

  @Column({ length: 20, unique: true })
  secretariatPhoneNumber: string;

  @Column({ length: 75 })
  picName: string;

  @Column({ length: 15 })
  picPhoneNumber: string;

  @ManyToOne(() => User, (user) => user.id)
  user: User;

  @ManyToOne(() => Village, (village) => village.id)
  village: Village;
}
