import EntityBaseModel from '../config/entity_base_model';
import { Entity, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { Event } from './event.entity';

@Entity()
export class Participation extends EntityBaseModel {
  @Column({ nullable: true })
  isAttend: boolean;

  @Column({ type: 'time with time zone', nullable: true })
  attendedAt: Date;

  @Column({ nullable: true })
  isWatch: boolean;

  @Column({ type: 'time with time zone', nullable: true })
  watchedAt: Date;

  @ManyToOne(() => Event, (event) => event.id)
  @ManyToOne(() => User, (user) => user.id)
  user: User;
}
