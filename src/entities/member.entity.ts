import { Village } from './villages.entity';
import { Gender } from '../helper/enum_list';
import EntityBaseModel from '../config/entity_base_model';
import { Entity, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Member extends EntityBaseModel {
  @Column({ length: 25, nullable: true })
  nia: string;

  @Column({ type: 'enum', enum: Gender, default: Gender.MALE })
  gender: Gender;

  @Column({ length: 100 })
  address: string;

  @Column({ length: 75 })
  picName: string;

  @Column({ length: 15 })
  picPhoneNumber: string;

  @ManyToOne(() => User, (user) => user.id)
  user: User;

  @ManyToOne(() => Village, (village) => village.id)
  village: Village;
}
