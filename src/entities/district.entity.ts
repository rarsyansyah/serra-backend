import EntityBaseModel from '../config/entity_base_model';
import { Entity, Column } from 'typeorm';

@Entity()
export class District extends EntityBaseModel {
  @Column({ length: 30 })
  name: string;

  @Column({ length: 40 })
  slug: string;

  @Column({ length: 5 })
  posCode: string;
}
