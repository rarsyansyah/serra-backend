import EntityBaseModel from '../config/entity_base_model';
import { Entity, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { Event } from './event.entity';

@Entity()
export class Comment extends EntityBaseModel {
  @Column({ length: 255 })
  content: string;

  @ManyToOne(() => Event, (event) => event.id)
  @ManyToOne(() => User, (user) => user.id)
  user: User;
}
