import EntityBaseModel from '../config/entity_base_model';
import { Entity, Column, ManyToOne } from 'typeorm';
import { District } from './district.entity';

@Entity()
export class Village extends EntityBaseModel {
  @Column({ length: 50 })
  name: string;

  @Column({ length: 60 })
  slug: string;

  @ManyToOne(() => District, (district) => district.id)
  district: District;
}
