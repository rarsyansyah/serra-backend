import EntityBaseModel from '../config/entity_base_model';
import { Entity, Column } from 'typeorm';

@Entity()
export class Role extends EntityBaseModel {
  @Column({ length: 25 })
  name: string;

  @Column({ length: 25 })
  slug: string;
}
