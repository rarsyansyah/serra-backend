import EntityBaseModel from '../config/entity_base_model';
import { Entity, Column, ManyToOne } from 'typeorm';
import { Role } from './role.entitiy';

@Entity()
export class User extends EntityBaseModel {
  @Column({ unique: true })
  username: string;

  @Column({ length: 20 })
  firstName: string;

  @Column({ length: 50 })
  lastName: string;

  @Column({ length: 15, unique: true })
  phoneNumber: string;

  @Column({ length: 50, unique: true })
  email: string;

  @Column({ length: 100 })
  salt: string;

  @Column({ length: 100 })
  password: string;

  @Column({ default: false })
  isActive: boolean;

  @Column({ length: 100, nullable: true })
  emailVerificationCode: string;

  @Column({ length: 100, nullable: true })
  resetPasswordCode: string;

  @Column({ length: 100 })
  profilePhoto: string;

  @ManyToOne(() => Role, (role) => role.id)
  role: Role;
}
