import { Seeder, Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { randomStringGenerator } from '@nestjs/common/utils/random-string-generator.util';
import { hashPassword } from '../helper/hash_password';

export default class UserSeeder implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const salt = randomStringGenerator();

    const users = [
      {
        username: 'arsy',
        firstName: 'Rizky',
        lastName: 'Arsyansyah Rinjani',
        phoneNumber: '6285878420368',
        email: 'admin@gmail.com',
        salt,
        password: await hashPassword('admin123', salt),
        is_active: true,
        role_id: 'ebd2d780-f05d-4a14-8679-07bb0dbd449d',
        version: 1,
        created_at: new Date(),
      },
      {
        username: 'satria',
        firstName: 'Satria',
        lastName: 'Rembang',
        phoneNumber: '6285878420367',
        email: 'pc@gmail.com',
        salt,
        password: await hashPassword('pc123', salt),
        is_active: true,
        role_id: 'd99efb28-2701-4338-bd7a-f8ad03651acd',
        version: 1,
        created_at: new Date(),
      },
      {
        username: 'yogi',
        firstName: 'Yogi',
        lastName: 'Priyatno',
        phoneNumber: '6285878420366',
        email: 'pc@gmail.com',
        salt,
        password: await hashPassword('pac123', salt),
        is_active: true,
        role_id: 'e6a1759c-57b9-4723-8c0a-ee6a0dfa67f9',
        version: 1,
        created_at: new Date(),
      },
      {
        username: 'deni',
        firstName: 'Deni',
        lastName: 'Widiyanto',
        phoneNumber: '6285878420365',
        email: 'pc@gmail.com',
        salt,
        password: await hashPassword('pr123', salt),
        is_active: true,
        role_id: 'db04af74-585b-401a-9b1f-3d2118e9ab99',
        version: 1,
        created_at: new Date(),
      },
      {
        username: 'andika',
        firstName: 'Andika',
        lastName: 'Nur Rizky',
        phoneNumber: '6285878420364',
        email: 'member@gmail.com',
        salt,
        password: await hashPassword('pr123', salt),
        is_active: true,
        role_id: '26a05b19-fec0-46ee-8a25-ecc57e87d05e',
        version: 1,
        created_at: new Date(),
      },
      {
        username: 'evelyn',
        firstName: 'Evelyn',
        lastName: '',
        phoneNumber: '6285878420363',
        email: 'public@gmail.com',
        salt,
        password: await hashPassword('pr123', salt),
        is_active: true,
        role_id: 'f81c810e-bbfb-40de-897f-71025de11823',
        version: 1,
        created_at: new Date(),
      },
    ];

    await connection
      .createQueryBuilder()
      .insert()
      .into('user')
      .values(users)
      .execute();
  }
}
