import { Role } from './../entities/role.entitiy';
import { Seeder, Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';

export default class UserSeeder implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(Role)
      .values([
        {
          id: 'ebd2d780-f05d-4a14-8679-07bb0dbd449d',
          name: 'Admin',
          slug: 'admin',
        },
        {
          id: 'd99efb28-2701-4338-bd7a-f8ad03651acd',
          name: 'Pimpinan Cabang',
          slug: 'pc',
        },
        {
          id: 'e6a1759c-57b9-4723-8c0a-ee6a0dfa67f9',
          name: 'Pimpinan Anak Cabang',
          slug: 'pac',
        },
        {
          id: 'db04af74-585b-401a-9b1f-3d2118e9ab99',
          name: 'Pimpinan Ranting',
          slug: 'pr',
        },
        {
          id: '26a05b19-fec0-46ee-8a25-ecc57e87d05e',
          name: 'Member',
          slug: 'member',
        },
        {
          id: 'f81c810e-bbfb-40de-897f-71025de11823',
          name: 'Public',
          slug: 'public',
        },
      ])
      .execute();
  }
}
